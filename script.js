function mostrarMeses(){
    let meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio",
                "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]
    var num=1;
    for (var i=0; i<=11; i++){
        alert("Mes "+num+": "+ meses[i])
        num=num+1;
    }
}

function mostrarProductos(){
    class Producto_alimenticio{
        constructor(code, name, price){
            this.code=code;
            this.name=name;
            this.price=price;
        }
        get print(){
            return this.imprimeDatos();
        }
        imprimeDatos(){
            document.write(
                "<div class='mostrar-producto'> codigo: " + this.code +
                "<br> nombre: "+this.name+
                "<br> precio: "+this.price+"</div><br>"
            );
        }
    }
    
    let productos = new Array(
        new Producto_alimenticio("111", "Azucar", "2"),
        new Producto_alimenticio("222", "Sal", "1"),
        new Producto_alimenticio("333", "Aceite", "1.50")
    );
    for (let i=0; i<productos.length; i++){
        console.log(productos[i].print);
    }
}



